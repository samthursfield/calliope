Listening to music
==================

The previous sections talk about how to make playlists of interesting music.
Now we want to listen to some of them.

.. toctree::
   :maxdepth: 2

   listening-to-music/resolving-content
   listening-to-music/exporting-playlists
   listening-to-music/copying-music-to-a-device
   listening-to-music/creating-mixes
