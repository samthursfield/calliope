Copying music to a device
=========================

The :mod:`calliope.sync` module and :command:`cpe sync` command can be used
to copy files in a playlist to another folder, useful for syncing a track
list to your phone or portable media player.
